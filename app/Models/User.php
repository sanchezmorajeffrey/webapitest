<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use HasFactory;

    protected $fillable = ["id", "password"];

    public function LoggedTimeUser()
    {
        return $this->hasMany(LoggedTimeUser::class, 'user_id');
    }
}

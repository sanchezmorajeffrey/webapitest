<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoggedTimeUser extends Model
{
    use HasFactory;
    protected $table = "loggedtime_users";
    protected $fillable = ["id"];

    public function User()
    {
        return $this->belongsTo(User::class, 'id');
    }
    protected $casts = [
        'status' => 'boolean',
        'created_at' => 'datetime:Y-m-d H:00',
        'updated_at' => 'datetime:Y-m-d H:00',
        'last_active' => 'datetime:Y-m-d H:00',
    ];
}

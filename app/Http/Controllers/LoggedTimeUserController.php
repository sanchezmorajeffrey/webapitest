<?php

namespace App\Http\Controllers;

use App\Http\Resources\LoggedTimeUserResource;
use App\Models\LoggedTimeUser;
use App\Http\Requests\StoreLoggetTimeUserRequest;
use App\Http\Requests\UpdateLoggetTimeUserRequest;

class LoggedTimeUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        //
        return LoggedTimeUserResource::collection(LoggedTimeUser::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreLoggetTimeUserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLoggetTimeUserRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LoggetTimeUser  $loggetTimeUser
     * @return \Illuminate\Http\Response
     */
    public function show(LoggetTimeUser $loggetTimeUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateLoggetTimeUserRequest  $request
     * @param  \App\Models\LoggetTimeUser  $loggetTimeUser
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLoggetTimeUserRequest $request, LoggetTimeUser $loggetTimeUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LoggetTimeUser  $loggetTimeUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoggetTimeUser $loggetTimeUser)
    {
        //
    }
}

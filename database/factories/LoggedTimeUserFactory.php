<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Generator as Faker;
use Illuminate\Support\Carbon;

class LoggedTimeUserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $randomCreate_at = Carbon::today()->subDays(rand(0, 365));

        $date = User::all()->first()->updated_at;
        $randomUpdate_at = $date->addDay(2);
        return [
            //
            'user_id'=>User::inRandomOrder()->first()->id,
            'last_active'=> User::all()->first()->updated_at,
            'status' => true,
            'created_at' => User::all()->first()->updated_at,
            'updated_at' => $randomUpdate_at,
        ];
    }
}

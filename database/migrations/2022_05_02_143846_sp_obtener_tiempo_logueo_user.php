<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class SpObtenerTiempoLogueoUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $procedure = "
        CREATE PROCEDURE spObtenerTiempoLogueoUser(IN fecha date)
        BEGIN
            declare fechaInicial varchar(50);
            declare fechaFinal varchar(50);
            set fechaInicial = concat(fecha, ' 00:00:00');
            set fechaFinal = concat(fecha, ' 23:59:59');
            SELECT u.id,
                   u.name  as 'Nombre',
                   u.email as 'Correo',
                   ifnull((select CONCAT(
                                          MOD(TIMESTAMPDIFF(HOUR, min(lu.created_at), fechaFinal), 24), ' horas y ',
                                          MOD(TIMESTAMPDIFF(MINUTE, min(lu.created_at), fechaFinal), 60), ' minutos '
                                      )
                           from loggedtime_users lu
                           where lu.created_at between fechaInicial and fechaFinal
                             and lu.status = 1
                             and lu.user_id = u.id), 'sin registro este dia')
                           as 'Tiempo en sesion'
            FROM users u;
        END;";

        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        $procedure="DROP PROCEDURE IF EXISTS spObtenerTiempoLogueoUser";
        DB::unprepared($procedure);
    }
}
